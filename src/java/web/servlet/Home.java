package web.servlet;

import api.modelo.Topico;
import api.servico.ServicoTopico;
import core.servico.ServicoTopicoImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("")
public class Home extends HttpServlet{
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp){
        ServicoTopico sTopico = new ServicoTopicoImpl();
        
        List<Topico> topicos = sTopico.findRecente();
        req.setAttribute("topicos", topicos);
        
        ServletContext sc = req.getServletContext();
        
        try{
            sc.getRequestDispatcher("/dynamic/jsp/index.jsp").forward(req, resp);            
        } catch (Exception e){}
    }
}
