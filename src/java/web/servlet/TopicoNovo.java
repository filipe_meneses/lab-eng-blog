package web.servlet;

import api.modelo.Topico;
import api.modelo.Usuario;
import api.servico.ServicoTopico;
import core.servico.ServicoTopicoImpl;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/topico/novo")
public class TopicoNovo extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        try {
            req.setCharacterEncoding("UTF-8") ;
        } catch (UnsupportedEncodingException ex) {}
        ServletContext sc = req.getServletContext();  
        HttpSession session = req.getSession(true);
        Usuario u = (Usuario) session.getAttribute("usuarioLogado");
        
        if (u == null || u.isUsuarioCadastrado()) {
            try {
                resp.sendRedirect(req.getContextPath() + "/topico/novo?erro");
            } catch (IOException ex) {
                Logger.getLogger(Autenticador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            String titulo = req.getParameter("titulo");
            String conteudo = req.getParameter("conteudo");

            ServicoTopico sTopico = new ServicoTopicoImpl();
            Topico t = sTopico.adicionaTopico(titulo, conteudo);

            if (t != null){
                try {
                    resp.sendRedirect(req.getContextPath() + "/topico/novo?sucesso");
                } catch (IOException ex) {
                    Logger.getLogger(Autenticador.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    resp.sendRedirect(req.getContextPath() + "/topico/novo?erro");
                } catch (IOException ex) {
                    Logger.getLogger(Autenticador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp){
        ServletContext sc = req.getServletContext();
        try{
            sc.getRequestDispatcher("/dynamic/jsp/topico-novo.jsp").forward(req, resp);            
        } catch (Exception e){}
    }
}
