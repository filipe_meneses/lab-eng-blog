package web.servlet;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/entrar")
public class Entrar extends HttpServlet{
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp){
        ServletContext sc = req.getServletContext();
        
        try{
            sc.getRequestDispatcher("/dynamic/jsp/entrar.jsp").forward(req, resp);            
        } catch (ServletException | IOException e){}
    }
}
