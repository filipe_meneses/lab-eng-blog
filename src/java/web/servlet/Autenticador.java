package web.servlet;

import api.modelo.Usuario;
import api.servico.ServicoUsuario;
import core.servico.ServicoUsuarioImpl;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/autenticar")
public class Autenticador extends HttpServlet {   
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        
        ServletContext sc = req.getServletContext();  
        HttpSession sessao = req.getSession(true);	   
        
        String email = req.getParameter("email");
        String senha = req.getParameter("senha");
        
        ServicoUsuario sUsuario = new ServicoUsuarioImpl();
        Usuario u = sUsuario.autenticarUsuario(email, senha);
        
        String caminhoRedirecionado = "/";
        
        if (u!= null){
            sessao.setAttribute("usuarioLogado", u);
            if (u.isProprietario()) {
                caminhoRedirecionado = "/gerencia";
            }
        } else {
            caminhoRedirecionado = "/entrar?acessoNegado";
        } 
        
        try {
            resp.sendRedirect(req.getContextPath() + caminhoRedirecionado);
        } catch (IOException ex) {
            Logger.getLogger(Autenticador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}