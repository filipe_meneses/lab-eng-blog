package web.servlet;

import api.modelo.Usuario;
import api.servico.ServicoTopico;
import core.servico.ServicoTopicoImpl;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/gerencia/topico/excluir")
public class TopicoExcluir extends HttpServlet{
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        HttpSession session = req.getSession(true);
        Usuario u = (Usuario) session.getAttribute("usuarioLogado");
        Long topicoId = new Long(Integer.parseInt(req.getParameter("id")));
        String urlResultado = "/gerencia";
        
        if (u != null && u.isProprietario()) {
            ServicoTopico sTopico = new ServicoTopicoImpl();
            if (!sTopico.excluir(topicoId)) {
                urlResultado = "/gerencia?erroTopico=" + req.getParameter("id");
            }
        }
        
        try {
            resp.sendRedirect(req.getContextPath() + urlResultado);
        } catch (Exception e){}    
    }
}
