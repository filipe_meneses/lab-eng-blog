package web.servlet;

import api.modelo.Comentario;
import api.modelo.Topico;
import api.modelo.Usuario;
import api.servico.ServicoComentario;
import api.servico.ServicoTopico;
import api.servico.ServicoUsuario;
import core.servico.ServicoComentarioImpl;
import core.servico.ServicoTopicoImpl;
import core.servico.ServicoUsuarioImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/gerencia")
public class Gerencia extends HttpServlet{
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp){
        HttpSession session = req.getSession(true);
        Usuario u = (Usuario) session.getAttribute("usuarioLogado");
        
        if (u == null || !u.isProprietario()) {
            try {
                resp.sendRedirect(req.getContextPath());
            } catch (Exception e){}
        } else {
            ServicoComentario sComentario = new ServicoComentarioImpl();
            List<Comentario> comentarios = sComentario.findTodos();
            req.setAttribute("comentarios", comentarios);
            
            ServicoTopico sTopico = new ServicoTopicoImpl();
            List<Topico> topicos = sTopico.findRecente();
            req.setAttribute("topicos", topicos);
            
            ServicoUsuario sUsuario = new ServicoUsuarioImpl();
            List<Usuario> usuarios = sUsuario.findTodos();
            req.setAttribute("usuarios", usuarios);
            
            ServletContext sc = req.getServletContext();
            try{
                sc.getRequestDispatcher("/dynamic/jsp/gerencia.jsp").forward(req, resp);            
            } catch (Exception e){}
        }
    }
}
