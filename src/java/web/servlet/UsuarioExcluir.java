package web.servlet;

import api.modelo.Usuario;
import api.servico.ServicoUsuario;
import core.servico.ServicoUsuarioImpl;
import java.util.Objects;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/gerencia/usuario/excluir")
public class UsuarioExcluir extends HttpServlet{
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        HttpSession sessao = req.getSession(true);
        Usuario u = (Usuario) sessao.getAttribute("usuarioLogado");
        Long usuarioId = new Long(Integer.parseInt(req.getParameter("id")));
        String urlResultado = "/gerencia";
        
        if (u != null && u.isProprietario()) {
            ServicoUsuario sUsuario = new ServicoUsuarioImpl();
            if (!sUsuario.excluir(usuarioId)) {
                urlResultado = "/gerencia?erroUsuario=" + req.getParameter("id");
            } else {
                if (Objects.equals(u.getId(), usuarioId)) {
                    sessao.removeAttribute("usuarioLogado");
                }
            }
        }
        
        try {
            resp.sendRedirect(req.getContextPath() + urlResultado);
        } catch (Exception e){}    
    }
}
