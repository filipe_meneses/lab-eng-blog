package web.servlet;

import api.modelo.Topico;
import api.modelo.Usuario;
import api.servico.ServicoTopico;
import core.servico.ServicoTopicoImpl;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/gerencia/topico/editar")
public class TopicoEditar extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        try {
            req.setCharacterEncoding("UTF-8") ;
        } catch (UnsupportedEncodingException ex) {}
        ServletContext sc = req.getServletContext();
        HttpSession sessao = req.getSession();
        Usuario u = (Usuario) sessao.getAttribute("usuarioLogado");
        
        Long id = new Long(req.getParameter("id"));
        ServicoTopico sTopico = new ServicoTopicoImpl();
        Topico t = sTopico.findById(id);
        
        Boolean redirecionaHome = u == null || !u.isProprietario()|| !t.isEditavel();
        String caminhoFinal = "";
        
        if (!redirecionaHome) {
            String titulo = req.getParameter("titulo");
            String conteudo = req.getParameter("conteudo");
            
            t.setTitulo(titulo);
            t.setConteudo(conteudo);
            
            Boolean atualizou = sTopico.update(t);
            
            req.setAttribute("topico", t);
            
            String uriResultado = atualizou ? "&sucesso" : "&erro";
            caminhoFinal = "/gerencia/topico/editar?id=" + t.getId () + uriResultado;
        }
        
        try {
            resp.sendRedirect(req.getContextPath() + caminhoFinal);
        } catch (IOException ex) {
            Logger.getLogger(Autenticador.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp){
        HttpSession session = req.getSession();
        Usuario u = (Usuario) session.getAttribute("usuarioLogado");
        ServletContext sc = req.getServletContext();
        
        ServicoTopico sTopico = new ServicoTopicoImpl();
        Long id = new Long(req.getParameter("id"));
        
        Topico t = sTopico.findById(id);
        req.setAttribute("topico", t);
        
        Boolean redirecionaHome = u == null || !u.isProprietario()|| !t.isEditavel();
                 
        if (redirecionaHome) {
            try {
                resp.sendRedirect(req.getContextPath());
            } catch (Exception e){}
        } else {
            try{
                sc.getRequestDispatcher("/dynamic/jsp/topico-editar.jsp").forward(req, resp);            
            } catch (Exception e){}    
        }
    }
}
