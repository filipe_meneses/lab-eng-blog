package web.servlet;

import api.modelo.Usuario;
import api.servico.ServicoComentario;
import core.servico.ServicoComentarioImpl;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/gerencia/comentario/excluir")
public class ComentarioExcluir extends HttpServlet{
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        
        
        HttpSession session = req.getSession(true);
        Usuario u = (Usuario) session.getAttribute("usuarioLogado");
        Long comentarioId = new Long(Integer.parseInt(req.getParameter("id")));
        String urlResultado = "/gerencia";
        
        if (u != null && u.isProprietario()) {
            ServicoComentario sComentario = new ServicoComentarioImpl();
            if (!sComentario.excluir(comentarioId)) {
                urlResultado = "/gerencia?erroComentario=" + req.getParameter("id");
            }
        }
        
        try {
            resp.sendRedirect(req.getContextPath() + urlResultado);
        } catch (Exception e){}    
    }
}
