package web.servlet;

import api.modelo.Comentario;
import api.modelo.Topico;
import api.modelo.Usuario;
import api.servico.ServicoComentario;
import core.servico.ServicoComentarioImpl;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/topico/comentario")
public class TopicoComentario extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        try {
            req.setCharacterEncoding("UTF-8") ;
        } catch (UnsupportedEncodingException ex) {}
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        
        HttpSession session = req.getSession();
        ServletContext sc = req.getServletContext();
        
        Long topicoId = new Long(Integer.parseInt(req.getParameter("topico_id")));
        String conteudo = req.getParameter("conteudo");
        Usuario usuario = (Usuario) session.getAttribute("usuarioLogado");
        
        ServicoComentario sComentario = new ServicoComentarioImpl();
        Comentario c = sComentario.adicionarComentario(topicoId, usuario, conteudo);
        
        if (c != null){
            resp.getWriter().write("{\"ok\": true, \"filtrado\": \"" + c.getConteudo() + "\"}");
        } else {
           resp.sendError(500);
        } 
    }
}
