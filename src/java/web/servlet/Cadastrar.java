package web.servlet;

import api.modelo.Usuario;
import api.servico.ServicoUsuario;
import core.servico.ServicoUsuarioImpl;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/cadastrar")
public class Cadastrar extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
        try {
            req.setCharacterEncoding("UTF-8") ;
        } catch (UnsupportedEncodingException ex) {}
        ServletContext sc = req.getServletContext();  
        HttpSession session = req.getSession(true);	   
        
        String email = req.getParameter("email");
        String nome = req.getParameter("nome");
        String apelido = req.getParameter("apelido");
        String senha = req.getParameter("senha");
        
        String resultado = "/";
        String erro = "/cadastrar?erroAoCadastrar";
        
        if (senha.isEmpty() || nome.isEmpty() || email.isEmpty() || apelido.isEmpty()) {
            resultado = erro;
        } else {
            ServicoUsuario sUsuario = new ServicoUsuarioImpl();
            Usuario u = sUsuario.cadastrarUsuario(email, senha, nome, apelido);

            if (u != null){
                session.setAttribute("usuarioLogado", u);
            } else {
                resultado = erro;
            }
        }
        
        try {
            resp.sendRedirect(req.getContextPath() + resultado);
        } catch (IOException ex) {
            Logger.getLogger(Autenticador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp){
        
        ServletContext sc = req.getServletContext();
        try{
            sc.getRequestDispatcher("/dynamic/jsp/cadastrar.jsp").forward(req, resp);            
        } catch (Exception e){}
    }
}
