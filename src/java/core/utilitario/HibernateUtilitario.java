package core.utilitario;

import api.modelo.Comentario;
import api.modelo.Identificador;
import api.modelo.Palavrao;
import api.modelo.Topico;
import api.modelo.Usuario;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtilitario {
    
    private static SessionFactory fabrica = null;
    
    public static SessionFactory getFabricaDeSessao() {
        if (fabrica != null) {
            return fabrica;
        }
        
        StandardServiceRegistry registro = null;
        
        try {
            StandardServiceRegistryBuilder registryBuilder = 
                new StandardServiceRegistryBuilder();

            Map<String, String> settings = new HashMap<>();
            settings.put("hibernate.connection.driver_class", "org.mariadb.jdbc.Driver");
            settings.put("hibernate.connection.url", "jdbc:mariadb://127.0.0.1:3306/blog");
            settings.put("hibernate.connection.username", "root");
            settings.put("hibernate.connection.password", "root");
            settings.put("hibernate.show_sql", "true");
            settings.put("hibernate.hbm2ddl.auto", "update");

            registryBuilder.applySettings(settings);

            registro = registryBuilder.build();

            MetadataSources sources;
            sources = new MetadataSources(registro)
                    .addAnnotatedClass(Palavrao.class)
                    .addAnnotatedClass(Comentario.class)
                    .addAnnotatedClass(Topico.class)
                    .addAnnotatedClass(Usuario.class);


            Metadata metadata;
            metadata = sources.getMetadataBuilder().build();

            fabrica = metadata.getSessionFactoryBuilder().build();
          } catch (Exception e) {
            System.out.println("SessionFactory creation failed");
            e.printStackTrace();
            if (registro != null) {
              StandardServiceRegistryBuilder.destroy(registro);
            }
          }
        return fabrica;
    }

    public static Session getSessao () {
        Session sessao = getFabricaDeSessao().openSession();
        return sessao;
    }

    public static void updateTupla(Identificador obj) {
        Session sessao = getFabricaDeSessao().openSession();

        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            sessao.update(obj);
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
    }

    public static Long criarTupla(Identificador obj) {
        Session sessao = getFabricaDeSessao().openSession();

        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            sessao.save(obj);
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        return obj.getId();
    }
}