package core.dao;

import core.utilitario.HibernateUtilitario;
import api.dao.TopicoDAO;
import api.modelo.Comentario;
import api.modelo.Topico;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TopicoDAOImpl implements TopicoDAO {

    @Override
    public Topico insert(Topico topico) {
        topico.setId(HibernateUtilitario.criarTupla(topico));
        return topico;
    }

    @Override
    public List<Topico> findRecente() {
        List topicos;
        Session sessao;
        
        sessao = HibernateUtilitario.getSessao();
        
        try {
            Query query = sessao.getNamedQuery("Topico.recentes");
            topicos = query.list();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        return topicos;
    }

    @Override
    public Topico update(Topico topico) {
        HibernateUtilitario.updateTupla(topico);
        return topico;
    }

    @Override
    public Topico findById(Topico topico) {
        Topico t;
        Session sessao = HibernateUtilitario.getSessao();
        
        try {
            t = (Topico)sessao.load(Topico.class, topico.getId());
            t.getComentarios(); // Lazy load comentários
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        return t;
    }

    @Override
    public void excluir(Topico topico) {
        Session sessao = HibernateUtilitario.getSessao();
        
        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            Topico t = (Topico)sessao.load(Topico.class, topico.getId());
            for (Comentario c: t.getComentarios()) {
                c.setTopico(null);
                c.setUsuario(null);
            };
            sessao.delete(t);
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
    }
    
    
}
