package core.dao;

import core.utilitario.HibernateUtilitario;
import api.dao.ComentarioDAO;
import api.modelo.Comentario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ComentarioDAOImpl implements ComentarioDAO {

    @Override
    public Comentario insert(Comentario comentario) {
        comentario.setId(HibernateUtilitario.criarTupla(comentario));
        return comentario;
    }

    @Override
    public List<Comentario> findTodos() {
        List comentarios;
        Session sessao;

        sessao = HibernateUtilitario.getSessao();
        
        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            Query query = sessao.createQuery("SELECT c FROM Comentario c");
            comentarios = query.list();
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        return comentarios;
    }

    @Override
    public void excluir(Comentario comentario) {
        Session sessao = HibernateUtilitario.getSessao();
        
        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            Comentario c = (Comentario)sessao.load(Comentario.class, comentario.getId());
            sessao.delete(c);
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
    }
}
