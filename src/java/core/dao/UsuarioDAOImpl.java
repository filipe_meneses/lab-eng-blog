/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.dao;

import core.utilitario.HibernateUtilitario;
import api.dao.UsuarioDAO;
import api.modelo.Comentario;
import api.modelo.Usuario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UsuarioDAOImpl implements UsuarioDAO {
    
    @Override
    public Usuario insert(Usuario usuario) {
        usuario.setId(HibernateUtilitario.criarTupla(usuario));
        return usuario;
    }

    @Override
    public Usuario findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario findByEmail(String email) {
        List<Usuario> usuarios;
        Session sessao;
        sessao = HibernateUtilitario.getSessao();
        
        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            Query query = sessao.getNamedQuery("Usuario.findByEmail");
            query.setString("email", email);
            usuarios = query.list();
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        if (usuarios.size() > 0) {
            return usuarios.get(0);    
        }
        return null;
        
    }

    @Override
    public List<Usuario> findTodos() {
        List<Usuario> usuarios;
        Session sessao;
        sessao = HibernateUtilitario.getSessao();
        
        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            Query query = sessao.getNamedQuery("Usuario.findTodos");
            usuarios = query.list();
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        return usuarios;
    }

    @Override
    public void excluir(Usuario usuario) {
        Session sessao = HibernateUtilitario.getSessao();
        
        try {
            Transaction transObj;
            transObj = sessao.beginTransaction();
            Usuario u = (Usuario)sessao.load(Usuario.class, usuario.getId());
            for (Comentario c: u.getComentarios()) {
                c.setTopico(null);
                c.setUsuario(null);
            };
            sessao.delete(u);
            transObj.commit();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
    }
}
