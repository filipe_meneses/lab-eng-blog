package core.dao;

import core.utilitario.HibernateUtilitario;
import api.dao.PalavraoDAO;
import api.modelo.Palavrao;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class PalavraoDAOImpl implements PalavraoDAO {

    @Override
    public List<Palavrao> findTodos() {
        List palavroes;
        Session sessao;
        
        sessao = HibernateUtilitario.getSessao();
        
        try {
            Query query = sessao.getNamedQuery("Palavrao.todos");
            palavroes = query.list();
        } finally {
            if (sessao.isOpen()) {
                sessao.close();
            }
        }
        
        return palavroes;
    }
}
