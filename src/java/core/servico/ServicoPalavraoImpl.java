package core.servico;

import api.modelo.Palavrao;
import api.servico.ServicoPalavrao;
import core.dao.PalavraoDAOImpl;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class ServicoPalavraoImpl implements ServicoPalavrao {
    
    private static ServicoPalavraoImpl instancia;
    private List<Palavrao> palavroes;
    
    public ServicoPalavraoImpl() {}
    
    public static ServicoPalavraoImpl getInstancia () {
        if (instancia != null) return instancia;
        instancia = new ServicoPalavraoImpl();
        return instancia;
    }

    private void carregarPalavroes() {
        PalavraoDAOImpl pDao = new PalavraoDAOImpl();
        palavroes = pDao.findTodos();
    }
    
    @Override
    public String filtrar(String texto) {
        if (texto == null) return "";
        String[] palavras = texto.split(" ");
        StringBuilder palavrasFiltradas = new StringBuilder();
        
        if (null == palavroes) carregarPalavroes();        
        if (palavroes.isEmpty()) return texto;
        
        for (String palavra: palavras) {
            for (Iterator<Palavrao> iterator = palavroes.iterator(); iterator.hasNext();) {
                Palavrao p = iterator.next();
                int n = p.getTamanho();
                String conteudo = p.getConteudo().toLowerCase();
                if (palavra.toLowerCase().contains(conteudo)) {
                    String asteriscos = String.join("", Collections.nCopies(n, "*"));
                    palavra = palavra.toLowerCase();
                    palavra = palavra.replaceAll(conteudo, asteriscos);
                }
            }
            
            palavrasFiltradas.append(palavra);
            palavrasFiltradas.append(" ");    
        }
        
        String palavraFiltrada = palavrasFiltradas.toString().trim();
        
        return palavraFiltrada;
    }
}
