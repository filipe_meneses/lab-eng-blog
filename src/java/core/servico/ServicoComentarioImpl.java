package core.servico;

import api.modelo.Comentario;
import api.modelo.Topico;
import api.modelo.Usuario;
import api.servico.ServicoComentario;
import core.dao.ComentarioDAOImpl;
import java.util.List;


public class ServicoComentarioImpl implements ServicoComentario {

    @Override
    public Comentario insert(Comentario comentario) {
        ComentarioDAOImpl cDao = new ComentarioDAOImpl();
        try {
            cDao.insert(comentario);    
        } catch (Exception e) {}
        
        return comentario;
    }

    @Override
    public List<Comentario> findTodos() {
        ComentarioDAOImpl cDao = new ComentarioDAOImpl();
        return cDao.findTodos();
    }

    @Override
    public boolean excluir(Long comentarioId) {
        ComentarioDAOImpl cDao = new ComentarioDAOImpl();
        try {
            Comentario c = new Comentario();
            c.setId(comentarioId);
            cDao.excluir(c);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Comentario adicionarComentario(Long topicoId, Usuario usuario, String conteudo) {
        ServicoComentario sComentario = new ServicoComentarioImpl();
        Comentario c = new Comentario(new Topico(topicoId), usuario, conteudo);
        sComentario.insert(c);
        if (c.getId() != null) {
            return c;
        }
        return null;
    }
}
