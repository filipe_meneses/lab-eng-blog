package core.servico;

import api.modelo.Topico;
import api.servico.ServicoTopico;
import core.dao.TopicoDAOImpl;
import java.util.List;


public class ServicoTopicoImpl implements ServicoTopico {
    @Override
    public List<Topico> findRecente() {
        TopicoDAOImpl tDao = new TopicoDAOImpl();
        return tDao.findRecente();
    }

    @Override
    public Topico findById(Long id) {
        TopicoDAOImpl tDao = new TopicoDAOImpl();
        Topico t = new Topico();
        t.setId(id);
        return tDao.findById(t);
    }

    @Override
    public Boolean update(Topico topico) {
        TopicoDAOImpl tDao = new TopicoDAOImpl();
        Boolean isSucesso = true;
        
        try {
            tDao.update(topico);    
        } catch (Exception e) {
            isSucesso = false;
        }
        
        return isSucesso;
    }

    @Override
    public boolean excluir(Long topicoId) {
        TopicoDAOImpl tDao = new TopicoDAOImpl();
        try {
            Topico t = new Topico();
            t.setId(topicoId);
            tDao.excluir(t);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Topico adicionaTopico(String titulo, String conteudo) {
        TopicoDAOImpl tDao = new TopicoDAOImpl();
        Topico t = new Topico(titulo, conteudo);
        
        try {
            tDao.insert(t);
        } catch (Exception e) {}
        
        if (t.getId() == null) {
            return null;
        }
        return t;
    }
}
