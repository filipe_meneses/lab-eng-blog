package core.servico;

import api.dao.UsuarioDAO;
import api.modelo.Usuario;
import api.servico.ServicoUsuario;
import core.dao.UsuarioDAOImpl;
import java.util.List;

public class ServicoUsuarioImpl implements ServicoUsuario {

    @Override
    public Usuario insert(Usuario usuario) {
        UsuarioDAOImpl uDao = new UsuarioDAOImpl();
        try {
            uDao.insert(usuario);    
        } catch (Exception e) {}
        
        return usuario;
    }

    @Override
    public Usuario findByEmail(String email) {
        UsuarioDAO uDao = new UsuarioDAOImpl();
        Usuario u = uDao.findByEmail(email);
        return u;
    }    

    @Override
    public List<Usuario> findTodos() {
        UsuarioDAO uDao = new UsuarioDAOImpl();
        return uDao.findTodos();
    }

    @Override
    public boolean excluir(Long usuarioId) {
        UsuarioDAO uDao = new UsuarioDAOImpl();
        try {
            Usuario u = new Usuario();
            u.setId(usuarioId);
            uDao.excluir(u);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Usuario autenticarUsuario(String email, String senha) {
        Usuario u = findByEmail(email);
        
        if (!u.getSenha().equals(Usuario.gerarHash(senha))) {
            return null;
        } else {
            return u;
        }
    }

    @Override
    public Usuario cadastrarUsuario(String email, String senha, String nome, String apelido) {
        Usuario u = new Usuario(email, senha, nome, apelido);
        insert(u);
        
        if (u.getId() == null) {
            return null;
        } else {
            return u;
        }
    }
}
