package api.dao;

import api.modelo.Comentario;
import java.util.List;

public interface ComentarioDAO {
    public Comentario insert(Comentario comentario);
    public List<Comentario> findTodos ();
    public void excluir(Comentario comentario);
    
}
