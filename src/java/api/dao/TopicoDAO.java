package api.dao;

import api.modelo.Topico;
import java.util.List;

public interface TopicoDAO {
    public Topico insert(Topico topico);
    public List<Topico> findRecente();
    public Topico findById(Topico topico);
    public Topico update(Topico topico);
    public void excluir(Topico topico);
}
