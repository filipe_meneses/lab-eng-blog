package api.dao;

import api.modelo.Palavrao;
import java.util.List;

public interface PalavraoDAO {
    public List<Palavrao> findTodos();
}
