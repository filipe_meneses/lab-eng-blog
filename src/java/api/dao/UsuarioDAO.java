package api.dao;

import api.modelo.Usuario;
import java.util.List;

public interface UsuarioDAO {
    public Usuario insert(Usuario usuario);
    public Usuario findById(Long id);
    public Usuario findByEmail(String email);
    public List<Usuario> findTodos();
    public void excluir(Usuario usuario);
}
