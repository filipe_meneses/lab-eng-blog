package api.modelo;

import core.servico.ServicoPalavraoImpl;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "topico")
@NamedQuery(name = "Topico.recentes", query = "SELECT t FROM Topico t ORDER BY t.id DESC")
public class Topico extends Identificador{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Long id;

    @Column (name = "titulo", nullable = false)
    @Type(type="text")
    private String titulo;
    
    @Column (name = "conteudo", nullable = false)
    @Type(type="text")
    private String conteudo;
    
    @Column (name = "data_criacao", nullable = false)
    private Date dataCriacao = new Date();
    
    @OneToMany(
            fetch=FetchType.EAGER,
            mappedBy="topico",
            cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    private Set<Comentario> comentarios = new HashSet<>();
    
    public Topico(){}
    public Topico(String titulo, String conteudo){    
        setTitulo(titulo);
        setConteudo(conteudo);      
    }
    public Topico(Long id, String titulo, String conteudo,
                   Date dataCriacao){              
        super(id);
        setTitulo(titulo);
        setConteudo(conteudo);
        setDataCriacao(dataCriacao);
    }

    public Topico(Long id) {
        super(id);
    }
    
    public boolean isEditavel () {
        return getComentarios() == null || getComentarios().isEmpty();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getConteudo() {
        return conteudo.replace("\\n", "\n");
    }

    public String getConteudoFormatado() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("<p>");
        sb.append(getConteudo().replace("\r\n\r\n", "</p><p>"));
        sb.append("</p>");
        
        return sb.toString();
    }

    public void setConteudo(String conteudo) {
        this.conteudo = ServicoPalavraoImpl.getInstancia().filtrar(conteudo).replace(System.getProperty("line.separator"), "\\n");
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }
    
    public String getDataCriacaoFormatada() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(dataCriacao);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Comentario> getComentarios() {
        return comentarios;
    }
    
    public ArrayList<Comentario> getComentariosOrdenados () {
        ArrayList<Comentario> c = new ArrayList<Comentario>();
        c.addAll(comentarios);
        c.sort((a, b) -> Long.compare(b.getId(), a.getId()));
        return c;
    }

    public void setComentarios(Set<Comentario> comentarios) {
        this.comentarios = comentarios;
    }
}
