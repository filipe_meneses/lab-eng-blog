package api.modelo;

import core.servico.ServicoPalavraoImpl;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "comentario")
public class Comentario extends Identificador{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="topico_id", nullable=false)
    private Topico topico;
    
    @Column (name = "conteudo", nullable = false)
    @Type(type="text")
    private String conteudo;
    
    @ManyToOne
    @JoinColumn(name="usuario_id", nullable=false)
    private Usuario usuario;
    
    @Column (name = "data_criacao", nullable = false)
    private Date dataCriacao = new Date();

    public Comentario(){}
    
    public Comentario(Topico topico, Usuario usuario, String conteudo) {
        setTopico(topico);
        setConteudo(conteudo);
        setUsuario(usuario);
    }

    public Comentario(Long id, Topico topico, String conteudo, Usuario usuario) {
        super(id);
        setId(id);
        setTopico(topico);
        setConteudo(conteudo);
        setUsuario(usuario);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Topico getTopico() {
        return topico;
    }

    public void setTopico(Topico topico) {
        this.topico = topico;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = ServicoPalavraoImpl.getInstancia().filtrar(conteudo);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }
    
    public String getDataCriacaoFormatada() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(dataCriacao);
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }
}
