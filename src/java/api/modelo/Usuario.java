package api.modelo;

import core.servico.ServicoPalavraoImpl;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "usuario", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" })})
@NamedQueries({
    @NamedQuery(name = "Usuario.findByEmail", query = "SELECT u FROM Usuario u WHERE u.email = :email"),
    @NamedQuery(name = "Usuario.findTodos", query = "SELECT u FROM Usuario u")
})
public class Usuario extends Identificador{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Long id;

    @Column (name = "nome", nullable = false)
    private String nome;
    
    @Column (name = "senha", nullable = false)
    private String senha;
    
    @Column (name = "apelido", nullable = false, unique = true)
    private String apelido;
    
    @Column (name = "email", nullable = false, unique = true)
    private String email;
    
    @Column (name = "papel", nullable = false)
    private Integer papel = PapelEnum.USUARIO_CADASTRADO;

    @OneToMany(fetch=FetchType.EAGER, mappedBy="usuario",cascade = CascadeType.PERSIST, orphanRemoval = true)
    private Set<Comentario> comentarios = new HashSet<>();
    
    public Usuario(){}
    public Usuario(String email, String senha,
                   String nome, String apelido){
        setNome(nome);
        setSenha(senha);
        setApelido(apelido);
        setEmail(email);              
    }
    public Usuario(Long id, String email, String senha,
                   String nome, String apelido){
        super(id);
        setNome(nome);
        setSenha(senha);
        setApelido(apelido);
        setEmail(email);             
    }
    
    public static String gerarHash (String senha) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] md5sum = md.digest(senha.getBytes());
            return String.format("%032X", new BigInteger(1, md5sum));
        } catch (NoSuchAlgorithmException ex) {
            return senha;
        }
    }
    
    public boolean isProprietario () {
        return Objects.equals(this.papel, PapelEnum.PROPRIETARIO);
    }
    
    public boolean isUsuarioCadastrado () {
        return Objects.equals(this.papel, PapelEnum.USUARIO_CADASTRADO);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = ServicoPalavraoImpl.getInstancia().filtrar(nome);
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha){
        this.senha = gerarHash(senha);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = ServicoPalavraoImpl.getInstancia().filtrar(apelido);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Integer getPapel() {
        return papel;
    }

    public void setPapel(Integer papel) {
        this.papel = papel;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nome=" + nome + ", senha=" + senha + ", apelido=" + apelido + ", email=" + email + ", papel=" + papel + '}';
    }

    public Set<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(Set<Comentario> comentarios) {
        this.comentarios = comentarios;
    }
}
