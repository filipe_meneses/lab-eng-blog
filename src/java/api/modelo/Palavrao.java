package api.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "palavrao")
@NamedQuery(name = "Palavrao.todos", query = "SELECT p FROM Palavrao p")
public class Palavrao extends Identificador{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Long id;
    
    @Column (name = "conteudo", nullable = false)
    private String conteudo;

    public Palavrao() {}
    public Palavrao(Long id, String conteudo) {
        setId(id);
        setConteudo(conteudo);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public int getTamanho() {
        return this.conteudo.length();
    }

    @Override
    public String toString() {
        return "Palavrao{" + "id=" + id + ", conteudo=" + conteudo + '}';
    }
}
