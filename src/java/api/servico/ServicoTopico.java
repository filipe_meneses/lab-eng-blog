package api.servico;

import api.modelo.Topico;
import java.util.List;

public interface ServicoTopico {
    public Boolean update(Topico topico);
    public Topico findById(Long id);
    public Topico adicionaTopico(String titulo, String conteudo);
    public List<Topico> findRecente();
    public boolean excluir(Long topicoId);
}
