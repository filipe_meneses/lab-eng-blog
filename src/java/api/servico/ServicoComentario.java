package api.servico;

import api.modelo.Comentario;
import api.modelo.Usuario;
import java.util.List;

public interface ServicoComentario {
    public Comentario insert(Comentario comentario);
    public Comentario adicionarComentario(Long topicoId, Usuario usuario, String conteudo);
    public List<Comentario> findTodos ();

    public boolean excluir(Long comentarioId);
}
