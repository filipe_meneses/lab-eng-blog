package api.servico;

import api.modelo.Usuario;
import java.util.List;

public interface ServicoUsuario {
    public Usuario insert(Usuario usuario);
    public Usuario findByEmail(String email);
    public Usuario autenticarUsuario(String email, String senha);
    public Usuario cadastrarUsuario(String email, String senha, String nome, String apelido);
    public List<Usuario> findTodos();
    public boolean excluir(Long topicoId);
}
