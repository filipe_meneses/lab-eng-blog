<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.Date"%>
<%@page import="api.modelo.Comentario"%>
<%@page import="api.modelo.Usuario"%>
<%@page import="api.modelo.Topico"%>
<% Usuario u = (Usuario)session.getAttribute("usuarioLogado"); %>
<% Topico t = (Topico)request.getAttribute("topico"); %>
<jsp:useBean id="topico" class="api.modelo.Topico" scope="request" />

<% if (t != null) { %>
<article class="blogTopico">
    <h1 class="blogTopicoTitulo">
        <%= t.getTitulo() %>
    </h1>
    <div class="blogTopicoMeta">
        Postado em <%= t.getDataCriacaoFormatada() %>
    </div>
    <div class="blogTopicoDescricao">
        <%= t.getConteudoFormatado() %>
    </div>
        <div class="blogTopicoComentarios" data-id="<%= t.getId() %>">
        <h2 class="blogTopicoComentariosTitulo">Comentários</h2>
        <c:choose>
            <c:when test="${topico.comentarios.size() == 0}">
                <div class="blogTopicoComentariosSemComentarios">
                    <p>Nenhum comentário.</p>
                </div>
                <div class="blogTopicoComentariosLista"></div>
            </c:when>
            <c:otherwise>
                <div class="blogTopicoComentariosLista">
                    <% for(Comentario c: t.getComentariosOrdenados() ){ %>
                        <div class="blogTopicoComentario">
                            <span class="blogTopicoComentarioDataCriacao"><%= c.getDataCriacaoFormatada() %></span>
                            <span class="blogTopicoComentarioAutor"><%= c.getUsuario().getApelido() %></span>
                            <p><%= c.getConteudo() %></p>
                        </div>
                    <% } %>
                </div>
            </c:otherwise>
        </c:choose>
        <% if (u != null) { %>
            <form class="blogTopicoComentariosFormulario">
                <label>
                    Comente:
                    <textarea name="mensagem"></textarea>
                </label>
                <button type="submit">Comentar</button>
            </form>
        <% } else { %>
            <p><a href="${pageContext.request.contextPath}/entrar">Entre</a> para comentar!</p>
        <% } %>
    </div>
</article>
<% }%>