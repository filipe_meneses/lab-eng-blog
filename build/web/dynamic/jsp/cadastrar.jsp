<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="./head.jsp" />
    <body class="paginaEntrar">
        <div class="container">
            <jsp:include page="./cabecalho.jsp" />
            <div class="blogCorpo">
                <form class="blogFormulario" action="cadastrar" method="POST">
                    <label>
                        E-mail
                        <input type="email" name="email" required/>
                    </label>
                    <label>
                        Nome
                        <input type="text" name="nome" required/>
                    </label>
                    <label>
                        Apelido
                        <input type="text" name="apelido" required/>
                    </label>
                    <label>
                        Senha
                        <input type="password" name="senha" required/>
                    </label>
                    <button class="blogFormularioBotao" type="submit">
                        Cadastrar
                    </button>
                </form>
                <% if(request.getParameter("erroAoCadastrar") != null ){ %>
                <div class="mensagem erro">
                    Não foi possível cadastrar o usuário.
                </div>    
                <% } %>
            </div>
        </div>
    </body>
</html>
