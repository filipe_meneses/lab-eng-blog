<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="./head.jsp" />
    <body class="paginaTopicoNovo">
        <div class="container">
            <jsp:include page="./cabecalho.jsp" />
            <div class="blogCorpo">
                <form class="blogFormulario" action="" method="POST">
                    <label>
                        Título
                        <input type="text" name="titulo" class="blogFormulario__entrada"/>
                    </label>
                    <label>
                        Conteúdo
                        <textarea name="conteudo" class="blogFormulario__entrada" rows="30"></textarea>
                    </label>
                    <button class="blogFormularioBotao" type="submit">
                        Adicionar
                    </button>
                </form>
                <% if(request.getParameter("erroAoCadastrar") != null ){ %>
                <div class="mensagem erro">
                    Não foi possível cadastrar o usuário.
                </div>    
                <% } %>
                
                <% if(request.getParameter("erro") != null ){ %>
                <div class="mensagem erro">
                    Não foi possível cadastrar o tópico.
                </div>    
                <% } %>
                <% if(request.getParameter("sucesso") != null ){ %>
                <div class="mensagem sucesso">
                    Tópico cadastrado com sucesso!
                </div>    
                <% } %>
            </div>
        </div>
    </body>
</html>
