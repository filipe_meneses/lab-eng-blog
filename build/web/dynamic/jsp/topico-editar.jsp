<%@page import="java.util.Date"%>
<%@page import="api.modelo.Comentario"%>
<%@page import="api.modelo.Usuario"%>
<%@page import="api.modelo.Topico"%>
<% Usuario u = (Usuario)session.getAttribute("usuarioLogado"); %>
<% Topico t = (Topico)request.getAttribute("topico"); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="./head.jsp" />
    <body class="paginaTopicoEditar">
        <div class="container">
            <jsp:include page="./cabecalho.jsp" />
            <% if(t != null) { %>
            <div class="blogCorpo">
                <% if(request.getParameter("erro") != null ){ %>
                <div class="mensagem erro">
                    Não foi possível editar o tópico.
                </div>    
                <% } %>
                <% if(request.getParameter("sucesso") != null ){ %>
                <div class="mensagem sucesso">
                    Tópico editado com sucesso!
                </div>    
                <% } %>
                <form class="blogFormulario" action="" method="POST">
                    <input type="hidden" name="id" class="blogFormulario__entrada" value="<%= t.getId() %>"/>
                    <label>
                        Título
                        <input type="text" name="titulo" class="blogFormulario__entrada" value="<%= t.getTitulo() %>"/>
                    </label>
                    <label>
                        Conteúdo
                        <textarea name="conteudo" class="blogFormulario__entrada" rows="30"><%= t.getConteudo() %></textarea>
                    </label>
                    <button class="blogFormularioBotao" type="submit">
                        Editar
                    </button>
                </form>
            </div>
            <% } else { %>
            
            <div class="blogCorpo">
                <div class="mensagem erro">
                    Tópico não encontrado.
                </div>    
            </div>
            <% } %>
        </div>
    </body>
</html>
