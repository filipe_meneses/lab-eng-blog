/* global jspCtx, jspUsuario */
//
// 6. Utilizar JavaScript (JS) no front end;
// 11 Utilizar métodos da JQuery;
$(document).ready(function () {
    // 7. Utilizar JS Closures;
    var menuMobilePlugin = function (configuracao) {
        var estaAberto = false;
        var ouvidorEstado = function () {};

        var $interruptor = $('<div class="' + configuracao.classeInterruptor + '">Menu</div>');
        var $sombra = $('<div class="' + configuracao.classeSombra + '"></div>');

        $interruptor.on('click', function () {
            ouvidorEstado(estaAberto = !estaAberto);
        });

        // 10. Utilizar JS para criar dinamicamente elementos no DOM.
        var cabecalhoDOM = document.querySelector(configuracao.elementoPai);
        cabecalhoDOM.appendChild($interruptor[0]);
        cabecalhoDOM.appendChild($sombra[0]);

        // 9. Utilizar JS event listeners;
        // 8. Utilizar JS DOM Handlers;
        $sombra[0].addEventListener('click', function () {
            ouvidorEstado(estaAberto = false);
        });

        return {
            getCabecalho: function () {
              return cabecalhoDOM;
            },
            estaAberto: function () {
                return estaAberto;
            },
            quando: function (acao, funcao) {
                if (acao === 'mudar') {
                    ouvidorEstado = funcao;
                }
            }
        };
    };

    var comentarioPorAjax = function (index, componente) {
        var $componente = $(componente);
        var $formulario = $($componente.find('.blogTopicoComentariosFormulario'));
        var $listaDeMensagens = $($componente.find('.blogTopicoComentariosLista'));
        var $botaoDeEnvio = $($formulario.find('[type="submit"]'));
        var $textareaMensagem = $($formulario.find('[name="mensagem"]'));
        var $mensagemComentariosVazios = $($componente.find('.blogTopicoComentariosSemComentarios'))

        var topicoId = $componente.data('id');
        var textoAntigoBotaoDeEnvio = $botaoDeEnvio.html();


        var gerarComentario = function (apelido, conteudo) {
            var configuracao = {hour: "2-digit", minute: "2-digit"};
            var dataCriacao = (new Date()).toLocaleDateString("pt-BR", configuracao);

            return '\
            <div class="blogTopicoComentario">\
                <span class="blogTopicoComentarioDataCriacao">' + dataCriacao + '</span>\
                <span class="blogTopicoComentarioAutor">' + apelido + '</span>\
                <p>' + conteudo + '</p>\
            </div>\
            ';
        };

        $formulario.on('submit', function (evento) {
            evento.preventDefault();
            var dados = {
                "conteudo": $textareaMensagem.val(),
                "topico_id": topicoId
            };
            var uri = jspCtx + '/topico/comentario';
            var $elementoMensagemErro = $('<div class="mensagem erro">Falha ao adicionar comentário</div>');
            var $mensagemDeErroExistente = $($formulario.find('.mensagem'));

            if ($mensagemDeErroExistente.length) {
                $mensagemDeErroExistente.remove();
            }
            $botaoDeEnvio.html("Enviando...");

            var quandoSucesso = function (resposta) {
                if ($mensagemComentariosVazios.length) {
                    $mensagemComentariosVazios.remove();
                }
                $textareaMensagem.val("");
                var $comentario = $(gerarComentario(jspUsuario.apelido, resposta.filtrado));
                $listaDeMensagens.prepend($comentario);
                $botaoDeEnvio.html(textoAntigoBotaoDeEnvio);
            };
            var quandoErro = function () {
                $formulario.append($elementoMensagemErro);
                $botaoDeEnvio.html(textoAntigoBotaoDeEnvio);
            };

            $.ajax({
                type: "POST",
                contentType: "application/x-www-form-urlencoded",
                url: uri,
                data: dados,
                success: quandoSucesso,
                error: quandoErro
             });
        });
    };

    var menu = menuMobilePlugin({
        elementoPai: '.blogCabecalho',
        classeInterruptor: 'blogNavInterruptor',
        classeSombra: 'blogNavSombra'
    });

    menu.quando('mudar', function (estaAberto) {
        $cabecalho = $(menu.getCabecalho());
        if (estaAberto) {
            $cabecalho.addClass('blogNavAtivo');
        } else {
            $cabecalho.removeClass('blogNavAtivo');
        }
    });

    $('.blogTopicoComentarios').each(comentarioPorAjax);

});
