<%-- 
    Document   : menu
    Created on : Sep 4, 2018, 7:36:53 AM
    Author     : filipe
--%>
<%@page import="api.modelo.EnumPapeis"%>
<%@page import="api.modelo.Papel"%>
<%@page import="api.modelo.Usuario"%>
<header>
    <nav>
        <ul class="navMenuList">
            <li> <a href="${pageContext.request.contextPath}/Adicionar.action"> Adicionar Contato </a> </li>
            <li> <a href="${pageContext.request.contextPath}/Remover.action"> Remover Contato </a></li>
            <li> <a href="${pageContext.request.contextPath}/Listar.action">Listar Contatos </a></li>
            <li> <a href="${pageContext.request.contextPath}/Alterar.action">Alterar Contatos </a></li>
            <% 
                for(Papel p: u.getPapeis()){
                    if( p.getDescricao().equals(EnumPapeis.PROPRIETARIO) ){
                        out.print("<li> ADMIN </li>");
                    }
                }
            %>
            <li> <a href="${pageContext.request.contextPath}">Sair</a></li>
        </ul>
    </nav>
</header>
