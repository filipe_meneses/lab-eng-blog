<%@page import="api.modelo.Usuario"%>
<% Usuario u = (Usuario)session.getAttribute("usuarioLogado"); %>
<head>
    <title>Blog</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/estilos.css"/>
    <script>
        var jspCtx = "${pageContext.request.contextPath}";
        var jspUsuario = {};
        <% if (u != null) { %>
        jspUsuario = {
            apelido: "<%= u.getApelido() %>"
        };
        <% }%>
    </script>
    <script src="${pageContext.request.contextPath}/static/js/lib/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/principal.js" charset="UTF-8"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
</head>