<%-- 
    Document   : entrar
    Created on : Sep 4, 2018, 4:17:12 PM
    Author     : filipe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="./head.jsp" />
    <body class="paginaEntrar">
        <div class="container">
            <jsp:include page="./cabecalho.jsp" />
            <div class="blogCorpo">
                <form class="blogFormulario" action="autenticar" method="POST">
                    <label>
                        E-mail
                        <input type="email" name="email" class="blogFormulario__entrada"/>
                    </label>
                    <label>
                        Senha
                        <input type="password" name="senha" class="blogFormulario__entrada"/>
                    </label>    
                    <button class="blogFormularioBotao" type="submit">
                        Entrar
                    </button>
                </form>
                <% if(request.getParameter("acessoNegado") != null ){ %>
                <div class="mensagem erro">
                        Usuário não existe ou credenciais estão incorretas.
                    </div>    
                <% } %>
            </div>
        </div>
    </body>
</html>
