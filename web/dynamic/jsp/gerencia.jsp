<%@page import="api.modelo.Usuario"%>
<%@page import="api.modelo.Topico"%>
<%@page import="api.modelo.Comentario"%>
<%@page import="java.util.List"%>
<% List<Comentario> comentarios = (List<Comentario>) request.getAttribute("comentarios"); %>
<% List<Topico> topicos = (List<Topico>) request.getAttribute("topicos"); %>
<% List<Usuario> usuarios = (List<Usuario>) request.getAttribute("usuarios"); %>
<% int erroTopico = -1; %>
<% int erroUsuario = -1; %>
<% int erroComentario = -1; %>
<%    
    if(request.getParameter("erroTopico") != null) {
        erroTopico = Integer.parseInt(request.getParameter("erroTopico"));
    }
    if(request.getParameter("erroUsuario") != null) {
        erroUsuario = Integer.parseInt(request.getParameter("erroUsuario"));
    }
    if(request.getParameter("erroComentario") != null) {
        erroComentario = Integer.parseInt(request.getParameter("erroComentario"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <jsp:include page="./head.jsp" />
    <body class="paginaEntrar">
        <div class="container">
            <jsp:include page="./cabecalho.jsp" />
            <div class="blogCorpo">
                <section class="blogGerenciaModulo">
                    <header class="blogGerenciaModuloCabecalho">
                        <h1>
                            Usuários
                        </h1>
                    </header>
                    <table>
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Nome</th>
                                <th>Apelido</th>
                            </tr>
                        </thead>
                        <tbody>
                        <% if(usuarios != null) { for(Usuario u: usuarios ){ %>
                            <tr>
                                <td><%= u.getEmail() %></td>
                                <td><%= u.getNome() %></td>
                                <td><%= u.getApelido() %></td>
                                <td>
                                    <% if (erroUsuario == u.getId()) { %>
                                        Erro ao excluir usuário
                                    <% } %>
                                    <form action="gerencia/usuario/excluir" method="POST">
                                        <input type="hidden" name="id" value="<%= u.getId() %>"/>
                                        <button type="submit" class="palido">
                                            Excluir
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <% }} %>
                        </tbody>
                    </table>
                </section>
                
                <section class="blogGerenciaModulo">
                    <header class="blogGerenciaModuloCabecalho">
                        <h1 class="blogGerenciaModuloCabecalhoTitulo">
                            Tópicos
                        </h1>
                        <a href="topico/novo" class="ancora">
                            Novo
                        </a>
                    </header>
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Título</th>
                                <th>Comentários</th>
                                <th>Data criação</th>
                            </tr>
                        </thead>
                        <tbody>
                        <% if(topicos != null) { for(Topico t: topicos ){ %>
                            <tr>
                                <td><%= t.getId() %></td>
                                <td><%= t.getTitulo() %></td>
                                <td><%= t.getComentarios().size() %></td>
                                <td><%= t.getDataCriacaoFormatada() %></td>
                                <td>
                                    <% if(t.isEditavel()){ %>
                                    <a href="gerencia/topico/editar?id=<%= t.getId() %>" class="botao enxuto">Editar</a>
                                    <% } %>
                                    <% if (erroTopico == t.getId()) { %>
                                        Erro ao excluir tópico
                                    <% } %>
                                    <form action="gerencia/topico/excluir" method="POST">
                                        <input type="hidden" name="id" value="<%= t.getId() %>"/>
                                        <button type="submit" class="palido">
                                            Excluir
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <% }} %>
                        </tbody>
                    </table>
                </section>
                
                <section class="blogGerenciaModulo">
                    <header class="blogGerenciaModuloCabecalho">
                        <h1>
                            Comentários
                        </h1>
                    </header>
                    <table>
                        <thead>
                            <tr>
                                <th>Tópico</th>
                                <th>Usuário</th>
                                <th>Conteúdo</th>
                                <th>Data criação</th>
                            </tr>
                        </thead>
                        <tbody>
                        <% if(comentarios != null) { for(Comentario c: comentarios ){ %>
                            <tr>
                                <td><%= c.getTopico().getTitulo() %></td>
                                <td><%= c.getUsuario().getApelido() %> (<%= c.getUsuario().getEmail() %>)</td>
                                <td><%= c.getConteudo() %></td>
                                <td><%= c.getDataCriacaoFormatada() %></td>
                                <td>
                                    <% if (erroComentario == c.getId()) { %>
                                        Erro ao excluir comentário
                                    <% } %>
                                    <form action="gerencia/comentario/excluir" method="POST">
                                        <input type="hidden" name="id" value="<%= c.getId() %>"/>
                                        <button type="submit" class="palido">
                                            Excluir
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <% }} %>  
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </body>
</html>
