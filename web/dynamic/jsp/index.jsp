<%@page import="java.util.ArrayList"%>
<%@page import="api.modelo.Topico"%>
<%@page import="api.modelo.Usuario"%>
<% Usuario u = (Usuario)session.getAttribute("usuarioLogado"); %>
<% ArrayList<Topico> topicos = (ArrayList<Topico>)request.getAttribute("topicos"); %>
<!DOCTYPE html>
<html>
    <jsp:include page="./head.jsp" />
    <body>
        <div class="container">
            <jsp:include page="./cabecalho.jsp" />
            <div class="blogCorpo">
                <section class="blogTopicos">
                    <% if (topicos == null || topicos.isEmpty()) { %>
                        <div class="mensagem">
                            <p>Esse blog est� vazio. </p>
                        </div>
                    <% } else { %>
                        <% for(Topico t: topicos ){ %>
                                <% request.setAttribute("topico", t); %>
                                <jsp:include page="./topico.jsp" />
                        <% } %>
                    <% } %>
                </section>
            </div>
        </div>
    </body>
</html>
